<?php

/**
 * Classe per il rendering dei template nell' MVC
 * @brief Classe per il rendering dei template nell' MVC
 *
 * @author Sergio Sicari
 */
class Template 
{

    private $registry;
    private static $_rendering;
    
    function __construct($registry) 
    {
        self::$_rendering = true;
        $this->registry = $registry;
    }
    
    /**
     * Realizza l' auto set delle variabili (magic method) da passare al template
     * @param string $index
     * @param string $value
     * @return void
     */
    function __set($index,$value) 
    {
        $this->vars[$index] = $value;
    }

    static function setRendering($value)
    {
        self::$_rendering = $value;
    }

    /**
     * Mostra il template $name
     * @param string $name
     * @throws Exception
     * @return bool
     */
    function show($name) 
    {
        $path = __SITE_PATH . APPLICATION_PATH . 'views' . '/' . $name . '.php';
        
        if (file_exists($path) == false)
        {
            throw new Exception('Template not found in '. $path);
            return false;
        }
        
        // Load variables
        foreach ($this->vars as $key => $value)
        {
            $$key = $value;
        }

        if(self::$_rendering)
            require_once ($path);

    }
    
}

