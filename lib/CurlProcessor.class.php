<?php

/**
 * cUrl Processor
 * @brief simple cUrl client to get/post request; it's a min version of https://github.com/sergioska/xcrawler
 * @author sergioska
 *
 */


class CurlProcessor{
	
	private $_hCurl;
	private $_sUrl;
	private $_sCookies;
	
	function __construct(){
		$this->_hCurl = null;
		$this->_sUrl = null;
		$this->_sCookies = null;
		$aOptions = array(
				CURLOPT_SSL_VERIFYPEER => true,
				CURLOPT_USERAGENT => "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6",
				CURLOPT_TIMEOUT => 60,
				CURLOPT_FOLLOWLOCATION => 1,
				CURLOPT_RETURNTRANSFER => 1,
				);
		$this->_hCurl = curl_init();
		curl_setopt_array($this->_hCurl, $aOptions);
	}
	
	/**
	 * Execute a cUrl post request
	 * @param array() $aParams
	 *  post params
	 *  Ex.: array('username_field' => 'blahblah', 'password_field' => ...', ...);
	 * @return mixed
	 */
	public function post($aParams=array()){
		$mResult = false;
		$sPostdata = "";
		if(!isset($this->_sUrl))
			return false;
		if(!empty($aParams)){
			foreach($aParams as $key => $value){
				$sPostdata .= $key . "=" . urlencode($value) . "&";
			}
			$sPostdata = substr($sPostdata, 0, -1);
			curl_setopt ($this->_hCurl, CURLOPT_POSTFIELDS, $sPostdata);
		}
		curl_setopt ($this->_hCurl, CURLOPT_COOKIE, $this->_sCookies);
		curl_setopt ($this->_hCurl, CURLOPT_REFERER, $this->_sUrl);
		curl_setopt ($this->_hCurl, CURLOPT_POST, 1);
		try{
			$mResult = curl_exec ($this->_hCurl);
		}catch(Exception $e){
			$sError     = curl_errno( $this->_hCurl );
			$sErrorMsg  = curl_error( $this->_hCurl );
			$mResult 	= $sErrorMsg;
		}
		//var_dump($mResult);
		return $mResult;
	}

	/**
	 * Execute a cUrl get request
	 * @param array() $aParams
	 *  get params
	 *  Ex.: array('username_field' => 'blahblah', 'password_field' => ...', ...);
	 * @return mixed
	 */
	public function get($aParams=array()){
		if(!empty($aParams)){
			foreach($aParams as $key => $value){
				$sGetdata .= $key . "=" . urlencode($value) . "&";
			}
			$sGetdata = substr($sPostdata, 0, -1);
			curl_setopt ($this->_hCurl, CURLOPT_REFERER, $this->_sUrl . "?" . $sGetdata);
		} else {
			curl_setopt ($this->_hCurl, CURLOPT_REFERER, $this->_sUrl);
		}		
		try{
			curl_setopt ($this->_hCurl, CURLOPT_POST, 0);
			curl_setopt ($this->_hCurl, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt ($this->_hCurl, CURLOPT_HEADER, 0);
			curl_setopt ($this->_hCurl, CURLOPT_COOKIE, $this->_sCookies);
			curl_setopt ($this->_hCurl, CURLOPT_RETURNTRANSFER, 1);
			$mResult = curl_exec ($this->_hCurl);
		}catch(Exception $e){
			echo "ERRORE";
			$sError     = curl_errno( $this->_hCurl );
			$sErrorMsg  = curl_error( $this->_hCurl );
			$mResult 	= $sErrorMsg;
		}
		//echo $mResult;
		return $mResult;
	}
	
	
	public function setCookie(){
		if(!isset($this->_sUrl))
			return false;
		$hCurl = curl_init($this->_sUrl);
		curl_setopt($hCurl, CURLOPT_URL, $this->_sUrl);
		curl_setopt($hCurl, CURLOPT_FOLLOWLOCATION, 0);
		curl_setopt($hCurl, CURLOPT_HEADER, true);
		curl_setopt($hCurl, CURLOPT_RETURNTRANSFER, 1);
		$sData = curl_exec($hCurl);
		curl_close($hCurl);
		preg_match_all('|Set-Cookie: (.*);|U', $sData, $aMatches);
		$sCookies = implode('; ', $aMatches[1]);
		$this->_sCookies = $sCookies;
	}
	
	public function getCookie(){
		return $this->_sCookies;
	}
	
	/**
	 * Close cUrl handler
	 */
	public function close(){
		if(isset($this->_hCurl))
			curl_close($this->_hCurl);
	}
	
	public function getUrl(){
		return $this->_sUrl;
	}
	
	public function setUrl($sUrl){
		curl_setopt ($this->_hCurl, CURLOPT_URL, $sUrl);
		$this->_sUrl = $sUrl;
	}
	
	public function getCurl(){
		return $this->_hCurl;
	}
}