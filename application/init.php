<?php

/**
 * this function set __autoload magic method
 */

require_once (__SITE_PATH . APPLICATION_PATH . 'config/app.inc');

function __autoload($class_name){
    /*** if class name contains 'Model' word search class file on application/models path ***/
    if (strpos($class_name, 'Model')) {
        if (file_exists($file=__SITE_PATH . APPLICATION_PATH . 'models/' . $class_name . '.class.php')) 
            require_once($file);
        else throw new Exception('model ' . $class_name . ' not found!');
    }
    /*** if class name contains 'Controller' word search class file on application/controllers path ***/
    elseif (strpos($class_name, 'Controller') && $class_name != 'baseController') {
        if (file_exists($file=__SITE_PATH . APPLICATION_PATH . 'controllers/' . $class_name . '.php'))
            require_once($file);
        else throw new Exception('controller ' . $class_name . ' not found!');
    }
    /*** if class name contains 'Entity' word search class file on application/models/entities path ***/
    elseif (strpos($class_name, 'Entity')) {
        if (file_exists($file=__SITE_PATH . APPLICATION_PATH . 'models/entities/' . $class_name . '.class.php'))
            require_once($file);
        else throw new Exception('entity ' . $class_name . ' not found!');
    }
    else {
    	/*** internal lib method start with __ ***/
        if (strpos($class_name, "__")===0) {
        	$struct = explode("_", $class_name);
            if (file_exists($file=__SITE_PATH . '/lib/' . $struct[3] . '/' . $class_name . '.class.php')) 
                require_once($file);
        }
        else if (file_exists($file=__SITE_PATH . '/lib/' . $class_name . '.class.php')) 
            require_once($file);
        else throw new Exception($class_name . ' not found!');
    }
}

/*** a new registry object ***/
$registry = new Registry();
//$session = SessionManager::getInstance();
