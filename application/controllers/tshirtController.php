<?php

/**
 * Class tshirtController
 * @brief Controller class to manage tshirt view action
 * @author Sergio Sicari
 */

class tshirtController extends baseController{

    /**
     * main view
     */
    public function index(){
		$oShirt = new ShirtModel();
		$_oCats = $oShirt->getCategories();
		$_aCats = $_oCats->result;
		$this->registry->template->title = "t-shirt";
		$this->registry->template->aCategories = $_aCats;
		$this->registry->template->show('tshirt');
	}

    /**
     * it is used by ajax category selector request
     */
    public function ajaxcatselector(){
		$oShirt = new ShirtModel();
		$_oCat = $oShirt->getCategory($_GET['id']);
		echo $_oCat;
	}

    /**
     * it is used by ajax product selector request
     */
	public function ajaxprodselector(){
		$oShirt = new ShirtModel();
		$_oCat = $oShirt->getProduct($_GET['id']);
		echo $_oCat;		
	}

}